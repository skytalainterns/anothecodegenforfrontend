package main.com.skytala;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class AllesMoegliche {
public static String path;
	public static void main(String args[]) {
		AllesMoegliche meins = new AllesMoegliche();
		Pars parser = new Pars();
		String servicename = "updateProductCategory";
		path = new File("").getAbsolutePath();
		
		try {
			PrintWriter pw = new PrintWriter(path+"/output.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		meins.printNames(servicename);
		meins.printTypes(servicename);
		meins.printOptional(servicename);
		
	}
	public void printTypes(String servicename){
		PrintWriter pw;
		FileReader fr;
		BufferedReader br;
		String line;
		String attr;
		Pars pars = new Pars();
		
		
		try {
		pw= new PrintWriter(new FileOutputStream(new File(path + "/output.txt"), true));
		fr = new FileReader(path+"/services.txt");
		br  = new BufferedReader(fr);
		
		pw.println("");
		pw.println("");
		
		while((line = br.readLine())!= null){
			if(line.contains("Servicename: " + '"' + servicename + '"')){
				while((line = br.readLine())!=null && !line.contains("Servicename:")){
					if(line.contains("Attr")){
						attr = pars.parsanything(line, "type:");
						if(!attr.equals("")){
							
							pw.println("typelist.AddLast("+'"'+attr+'"'+");");
							
						}
						
					}
					
				}
				
			}
			
		}
		pw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void printOptional(String servicename){
		PrintWriter pw;
		FileReader fr;
		BufferedReader br;
		String line;
		String attr;
		Pars pars = new Pars();
		
		try {
			pw= new PrintWriter(new FileOutputStream(new File(path + "/output.txt"), true));
			pw.println("");
			pw.println("");
		fr = new FileReader(path+"/services.txt");
		br  = new BufferedReader(fr);
		
		while((line = br.readLine())!= null){
			if(line.contains("Servicename: " + '"' + servicename + '"')){
				while((line = br.readLine())!=null && !line.contains("Servicename:")){
					if(line.contains("Attr")){
						attr = pars.parsanything(line, "optional:");
						if(!attr.equals("")){
							if(attr.equals("true")){
							pw.println("optionallist.AddLast("+'"'+"-"+'"'+");");
							}else{
								pw.println("optionallist.AddLast("+'"'+"REQUIRED"+'"'+");");
							}
						}
						
					}
					
				}
				
			}
			
		}
		pw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	public void printNames(String servicename) {
		PrintWriter pw;
		FileReader fr;
		BufferedReader br;
		String line;
		String attr;
		Pars pars = new Pars();
		
		try {
			pw= new PrintWriter(new FileOutputStream(new File(path + "/output.txt"), true));
			pw.println("");
			pw.println("");
		fr = new FileReader(path+"/services.txt");
		br  = new BufferedReader(fr);
		
		while((line = br.readLine())!= null){
			if(line.contains("Servicename: " + '"' + servicename + '"')){
				while((line = br.readLine())!=null && !line.contains("Servicename:")){
					if(line.contains("Attr")){
						attr = pars.parsanything(line, "Attr:");
						if(!attr.equals("")){
							pw.println("parameterlist.AddLast("+'"'+attr+'"'+");");
							
						}
						
					}
					
				}
				
			}
			
		}
		pw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
	}

}
